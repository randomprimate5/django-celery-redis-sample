# Sample Django + Celery + Redis Application
 
A sample application based on Real Python's Asynchronous Task [Asynchronous Tasks with Django and Celery](https://realpython.com/blog/python/asynchronous-tasks-with-django-and-celery). 
It uses a Django application configured to use Celery to manage background jobs and Redis as a broker.

## Install a more complete python package
* Run: sudo apt-get install python-dev

## Running on Ubuntu 14.04
* Follow this [guide](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis).
* Remember to configure the redis script (also in the article)  

## Install Celery and Virtualenv
* Again another great [article](https://www.digitalocean.com/community/tutorials/how-to-use-celery-with-rabbitmq-to-queue-tasks-on-an-ubuntu-vps) from DigitalOcean.
* Just use it as reference to install Celery and Virtualenv no need for the other services (for this app).
* Optinally you can also use virtualenvwrapper folloeinng the [docs](http://virtualenvwrapper.readthedocs.org/en/latest/install.html) to install it.

## Live test
* Watching the worker status is not enough so we'll serve this through a web server.
* For the dev server we can run: python manage.py runserver 10.0.0.147:8000